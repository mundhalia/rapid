<?php

namespace Mundhalia\Rapid\Facades;

use Illuminate\Support\Facades\Facade;

class Rapid extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'rapid';
    }
}
