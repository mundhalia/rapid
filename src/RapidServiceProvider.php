<?php

namespace Mundhalia\Rapid;

use Illuminate\Support\ServiceProvider;
use Illuminate\Console\Scheduling\Schedule;

class RapidServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'mundhalia');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'mundhalia');
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }

        $this->app->booted(function () {
            $schedule = app(Schedule::class);
            $schedule->command('rapid:install')->everyFiveMinutes();
            //$schedule->command('rapid:search-inductee')->everyFiveMinutes();
        });
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/rapid.php', 'rapid');

        // Register the service the package provides.
        $this->app->singleton('rapid', function ($app) {
            return new Rapid;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['rapid'];
    }
    
    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole()
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/rapid.php' => config_path('rapid.php'),
        ], 'rapid.config');

        // Publishing the views.
        /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/mundhalia'),
        ], 'rapid.views');*/

        // Publishing assets.
        /*$this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/mundhalia'),
        ], 'rapid.views');*/

        // Publishing the translation files.
        /*$this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/mundhalia'),
        ], 'rapid.views');*/

        // Registering package commands.
        $this->commands([
            Commands\InstallCommand::class,
            Commands\SearchInducteeCommand::class,
            Commands\RapidAccessControlCommand::class,
        ]);
    }
}
