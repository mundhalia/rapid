<?php

namespace Mundhalia\Rapid;
use Mundhalia\Rapid\Models\Inductee;
use Carbon\Carbon;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Cache;

class Rapid
{
    public function rapidData(){
        return Inductee::select(
                            'learnerType.name',
                            'name',
                            'email',
                            'mobile'
                        )
                        ->whereIn('inducteeId', $this->getNewInductees())->get();
    }

    private function getNewInductees(){
        $date = $this->lastModifiedDate();
        $data = ['modifiedDateFrom' => $date];
        try{
            $response = Curl::to(config('rapid.token.host').'Inductee/Search')
                            ->withData(json_encode($data))
                            ->withHeader("Authorization: Bearer ".config('rapid.token.token'))
                            ->post();

            $response = json_decode($response, true);
            $data = array();
            foreach ($response['collection'] as $row) {
                unset($row['$type']);
                unset($row['learnerType']['$type']);
                unset($row['customFields']['$type']);
                $row['created']  = Carbon::parse($row['created']);
                $row['modified'] = Carbon::parse($row['modified']);
                Inductee::updateOrCreate(
                    ['inducteeId' => $row['inducteeId']],
                    $row
                );
                $data[] = (int)$row['inducteeId'];
            }
            return $data;
            //Cache::store('redis')->put('inductee', $response, now()->addMinutes(15)); // 10 Minutes
        }
        catch(Exception $e){
            return $e;
            report($e);
        }
    }

    private function lastModifiedDate(){
        return Inductee::select('modified')->orderby('modified.date', 'desc')->first();
    }
}