<?php

namespace Mundhalia\Rapid\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Inductee extends Model
{
    protected $connection= 'mongodb';
    
    protected $table = 'rapid_inductee';

    protected $guarded = ['id'];
}
