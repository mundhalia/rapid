<?php

namespace Mundhalia\Rapid\Commands;

use Illuminate\Console\Command;
use AccessControl;
use Rapid;
use Carbon\Carbon;
use Propaganistas\LaravelPhone\PhoneNumber;

class RapidAccessControlCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rapid:access-control';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Middleware command that joins CS Technologies to Rapid Systems';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $rapids = Rapid::rapidData();
        foreach ($rapids as $rapid) {
            if($rapid != null && $rapid->mobile != null){
                $rapid->mobile = $this->mobileFormat($rapid->mobile);
                AccessControl::createAccess($rapid->name, $rapid->learnerType['name'], Carbon::now(), Carbon::now()->addminutes(10), $rapid->email, $rapid->mobile);
            }
            else{
                $this->info($rapid->learnerType['name']);
                $this->comment('Email or Mobile missing');
            }
        }
    }

    private function mobileFormat($mobile)
    {
        try {
            return PhoneNumber::make($mobile, 'AU');
        } catch (\Execption $e) {
            report($e);
        }
    }
}
