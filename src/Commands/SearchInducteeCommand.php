<?php

namespace Mundhalia\Rapid\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SearchInducteeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rapid:search-inductee';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Search of the inductee';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    }

}
