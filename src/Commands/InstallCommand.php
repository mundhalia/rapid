<?php

namespace Mundhalia\Rapid\Commands;

use Illuminate\Console\Command;
use Ixudra\Curl\Facades\Curl;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Mundhalia\Rapid\Notifications\KeyGeneratedNotification;
use Notification;

class InstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rapid:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install rapid global token into .env';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(config('rapid.token.expiry') <= Carbon::now()->addMinutes(5)->timestamp){
            try{
                $response = Curl::to(config('rapid.key.host').'token')
                                ->withData(array(
                                                    'grant_type' => 'password', 
                                                    'username'   => config('rapid.key.username'),
                                                    'password'   => config('rapid.key.password'),
                                                ))
                                ->post();
                
                //$response = '{"access_token":"RfjambE6-lu94vqo-_hcjwUPHiRiXn8X7Au07pW_mGKlRvkh4B3U9FF8C42VK7WYP39sfjyEBWvEwmtR9xJFOsovETKSS6n_-J6iQMNgLG4HI5_yyFteFexp7ergGPoFrOlEes2KQXhimw1ZRytmMo7zBj3fZiysPLTTs985bmxZo9UdyFyPS4Gxes__50WB7FLc8sekgQKDE22w3JHeZ1Gbqk42x_0ha6DI0s5leylDkLFEy8wI2ePWC6muymiZCRTp0KRFYuQDpCNiXwhp2b-iAZ5krfKO_XblAN9qtnNsA-5fWPOxvaXSECYuKwWqxRvd4d00J-1BvLtfk5SQPcHhu4Hu0crJmebVeV-IwlM5uIIdmb8MREdrnD77bcmjmgK0FnV1BluPKqahZpT2T_In04SnTt230XGwJucNd95txoFgPoITNtTv50_N30M_xJv0-g","token_type":"bearer","expires_in":86399}';
                $response = json_decode($response);
                dump($response);
                if(isset($response->expires_in) && isset($response->access_token)){
                    $expiry = Carbon::now()->addseconds($response->expires_in)->timestamp;
                    //dd($response->access_token);
                    $this->putPermanentEnv('RAPID_TOKEN', $response->access_token);
                    $this->putPermanentEnv('EXPIRES_IN', $expiry);
                    $message = "Key Updated ".json_encode($response);
                }
                else{
                    $message = "No access token recieved ".json_encode($response);
                }

            }
            catch(Exception $e){
                $message = "Execption ".$e;
                report($e);
            }
            Notification::route('mail', config('rapid.email.to_developer_email'))->notify(new KeyGeneratedNotification($message));
            Log::info($message);
        }
        else{
            $this->info('Token still good to use');
        }
    }

    private function putPermanentEnv($key, $value)
    {
        $path = app()->environmentFilePath();
    
        $escaped = preg_quote('='.env($key), '/');
    
        file_put_contents($path, preg_replace(
            "/^{$key}{$escaped}/m",
            "{$key}={$value}",
            file_get_contents($path)
        ));
    }
}
