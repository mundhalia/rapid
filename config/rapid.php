<?php

return [
    'key' => [
        'host'     => env('RAPID_HOST'),
        'username' => env('RAPID_USERNAME'),
        'password' => env('RAPID_PASSWORD'),
    ],
    'token' => [
        'host'   => env('RAPID_URL'),
        'token'  => env('RAPID_TOKEN'),
        'expiry' => env('EXPIRES_IN'),
    ],
    'email' => [
        'from_email' => env('RAPID_FROM_EMAIL'),
        'from_email_name' => env('RAPID_FROM_EMAIL_NAME'),
        'to_developer_email' => env('RAPID_FROM_EMAIL_NAME'),
    ],
];